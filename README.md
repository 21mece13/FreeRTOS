Download and install FreeRTOS for shakti
========================================

git clone https://gitlab.com/shaktiproject/software/FreeRTOS.git --recurse-submodules

Please ensure, you installed the risc-v toolchain and exported them to 'PATH' variable.

vajra SoC
=========

cd FreeRTOS/FreeRTOS/Demo/shakti/vajra

make

pinaka SoC
=========

cd FreeRTOS/FreeRTOS/Demo/shakti/pinaka

make

parashu SoC
=========

cd FreeRTOS/FreeRTOS/Demo/shakti/parashu

make
